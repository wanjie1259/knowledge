## 智慧家居开发样例
### 简介
讲述本仓库托管的样例场景，用于开发者很好的区分哪种类型的开发样例可以放在本仓库
### 样例汇总
#### 连接模组类应用
- [l1x](docs/sm_flower_machine/README_zh.md)
- [l2x](docs/smart_lamp/README.md)
#### 带屏IoT设备应用
- [p1x](docs/sm_flower_machine/README_zh.md)
#### Camera应用
- [c1x](docs/smart_door_viewer_3516/README.md)
#### 标准设备应用
- [s1x](FA/DistSchedule/README_zh.md)
- [s2x](FA/DistScheduleEts)

### 参考资料
+ [refxx1]()
+ [refxx2]()

