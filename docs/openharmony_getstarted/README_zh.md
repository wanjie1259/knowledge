# OpenHarmony上手成长指南
作为一个从小白走过来的开发者，笔者认为想要遨游OpenHarmony社区，可以通过以下路径走下去。
![](./media/flow.png)

## 学会和社区对话

在社区中，可能没有集中的微信/QQ群，但一定有一个统一的发声渠道，那就是ISSUE/PR;指导文档有问题，提ISSUE；开发样例的实操表现达不到预期，提ISSUE；系统组件有缺陷，提ISSUE；你觉得应该有哪些规划，提ISSUE；一言以蔽之，你有任何问题（基于技术）都可以提ISSUE，这就是问。作为开发者不仅要会问，也要尝试答，开发者提出了那么多问题，有没有你能解答的？任何小小的回答都是为社区奉献，当然问其实也是贡献。有问有答，才会让社区不断的往前走,否则有种到此为止的感觉。因此开发者首先要做的就是学会提ISSUE/PR,毕竟学会问问题之后，后面遇到问题就好解决了。
![](./media/ISSUE_PR.png)
**简单三步即可搞定：**
+ [注册一个账号：获取对话的身份](./register_account/readme.md)
+ [创建一个ISSUE：学会提问题](./create_issue/readme.md)
+ [推送一个PR：学会提交答案](./push_pr/readme.md)

通过上述内容，你可以学会如何在社区发言，如何做贡献。
## 上手开发样例
如何搭建开发环境、拉取源码、运行开发样例始终是开发者的起步之路。学会通过官方文档排查问题永不过时。
+ **学会运行一个“Hello world”** 
    - [润和Hi3861开发板轻量系统快速上手(使用DevEco Device Tool)](https://growing.openharmony.cn/mainPlay/learnPathMaps?id=24)
    - [润和Hi3516DV300开发板小型系统快速上手(使用DevEco Device Tool)](https://growing.openharmony.cn/mainPlay/learnPathMaps?id=25)
    - [润和Hi3516DV300开发板标准系统快速上手(使用DevEco Device Tool)](https://growing.openharmony.cn/mainPlay/learnPathMaps?id=26)
    - [润和RK3568开发板标准系统快速上手(使用DevEco Device Tool)](https://growing.openharmony.cn/mainPlay/learnPathMaps?id=27)
    - [欧智通BES2600WM开发板轻量系统快速上手](https://growing.openharmony.cn/mainPlay/learnPathMaps?id=17)
    - [润和Hi3861开发板轻量系统快速上手(非IDE)](https://growing.openharmony.cn/mainPlay/learnPathMaps?id=18)
    - [小熊派Hi3861开发板轻量系统快速上手](https://growing.openharmony.cn/mainPlay/learnPathMaps?id=19)
    - [润和Hi3516DV300开发板小型系统快速上手](https://growing.openharmony.cn/mainPlay/learnPathMaps?id=20)
    - [润和Hi3518EV300开发板小型系统快速上手](https://growing.openharmony.cn/mainPlay/learnPathMaps?id=21)
    - [润和Hi3516DV300开发板标准系统快速上手](https://growing.openharmony.cn/mainPlay/learnPathMaps?id=22)
    - [润和RK3568开发板标准系统快速上手](https://growing.openharmony.cn/mainPlay/learnPathMaps?id=23)
    - [OpenHarmony IoT设备开发](https://growing.openharmony.cn/mainPlay/learnPathMaps?id=44)
    - [OpenHarmony硬件开发环境搭建DevEco Device Tool](https://growing.openharmony.cn/mainPlay/learnPathMaps?id=45)
+ **学会导入开发样例并运行**
    - [OpenHarmony app_samples:主干仓库针对OpenHarmony特性的](https://gitee.com/openharmony/app_samples)
    - [OpenHarmony codelabs:引导式开发样例，按步骤实操](https://gitee.com/openharmony/codelabs)
    - [OpenHarmony 场景Demos:看到面对生活各种场景的Demo](https://gitee.com/openharmony-sig/knowledge)
+ **学会查找权威资料**
     - [OpenHarmony官方文档仓库](https://gitee.com/openharmony/docs)
     - [OpenHarmony官网文档](https://openharmony.cn/docs?navId=3&navName=OpenHarmony%E6%96%87%E6%A1%A3%E6%80%BB%E8%A7%88)

## 参加社区活动
每个人都有一个侠客梦，当学有所成之后，看看是否能够“仗剑走天涯”，那么OpenHarmony的各项赛事无疑会是你最好的练兵场。参加社区活动，秀出自己的风采；优秀的案例将会被推送到知识体系开发样例仓库，经过评审流程进入OpenHarmony官网展示。可以定期的查看知识体系联合运营组举办的各种[赛事活动](https://gitee.com/openharmony-sig/online_event)。
+ [高校开发者成长计划](https://gitee.com/openharmony-sig/online_event/blob/master/college_growth_program/readme.md)
+ [开源贡献者计划2022](https://gitee.com/openharmony-sig/online_event/blob/master/Contributor_program/README.md)
+ [解决方案学生挑战赛](https://gitee.com/openharmony-sig/online_event/blob/master/solution_student_challenge/readme.md)


## 成为社区达人
我们从OpenHarmony社区学会了很多知识，那么如何回馈社区？记住，问题和答案都算是对社区的回馈！如果你有些小作品，不妨提交给知识体系，秀出自己的风采！
+ [手把手教你提交一个开发样例到社区](./co-helloworld_demos/README_zh.md)
+ [手把手教你提交一个三方库到社区](./port_thirdparty/README.md)
+ [参加或者组织SIG](https://gitee.com/openharmony-sig)

## 参考资料
+ 知识赋能课程:[如何成为OpenHarmony贡献达人](https://www.bilibili.com/video/BV1V44y1u7Br?spm_id_from=333.999.0.0)
+ 官方文档：[提交PR常规操作](https://gitee.com/openharmony-sig/sig-content/blob/master/Infrastructure/docs/manual/gitee%E6%8F%90pr%E5%B8%B8%E8%A7%84%E6%93%8D%E4%BD%9C.md)