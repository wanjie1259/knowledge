# gitee账号
## 注册gitee账号
+ 首先你准备一个自己的手机以及邮箱，为方便后续的操作方便，该手机号码以及邮箱没有和gitee平台发生关联。

+ 在[gitee官网](https://gitee.com/)完成注册
  ![](media/gitee官网.png)

+ 根据自己情况设置信息，图里所示个人空间地址很重要，它就是你的用户名。姓名可以是相同的，但用户名是唯一的。
  ![](media/注册信息填写.png)

+ **如无意外则创建成功**
  ![](media/账号创建成功.png)

## 账号信息补充

  在注册账号的时候，有些信息我们是没有补充的，基本信息基本只有电话号和密码，能够满足我们的登录并使用该账号“发言”。那么问题来了，为了让我们后续发言更有说服力，我们需要补充相关的信息。
  ![](./media/个人设置界面.png)

+  **补充邮箱**

  ![](./media/设置邮箱信息.png)
  按照图示补充邮箱信息，并设置自己的提交邮箱。为自己后续的打怪升级做准备，人民会记住你的贡献的。
  ![](./media/邮箱设置完毕.png)

## 签署DCO（这个很重要，一定要签署 ！！）

  可以到[DCO查询签署页面](https://dco.openharmony.cn/sign#/check-sign-status)查看一下是否签署，如果没有签署，需要签署。
  ![](./media/DCO查询签署界面.png)
  可先查询自己配置的gitee账号邮箱是否签署：
  ![](./media/DCO未签署.png)
  按照提示完成DCO签署动作
  ![](./media/DCO签署界面.png)
  再次查询自己配置的gitee账号邮箱可以看到已经完成签署
  ![](./media/DCO签署完成.png)
  问题:不签署DCO会有什么问题？

  你在OpenHarmony社区提交的PR会因为流程中的DCO check不通过，导致PR提交失败。
  ![](./media/DCO检查失败.png)

+ 从此以后你就有了一个自己的账号,gitee江湖上已经有了你的身影;但是显然，此时我们这个角色啥也没有，类似刚创建完的游戏角色一样，修行练级之旅已然开始。

  ![](media/交流界面.png)

## 遨游OpenHarmony社区
看看江湖的样子。gitee上有很多内容，你可以根据自己的需求到处逛逛。OpenHarmony是一个大的社区，主要有三个组织：
+ [https://gitee.com/openharmony](https://gitee.com/openharmony)，OpenHarmony主干，这个包含很多仓库
+ [https://gitee.com/openharmony-sig](https://gitee.com/openharmony), OpenHarmony SIG组织，用于孵化OpenHarmony相关开源生态
+ [https://gitee.com/openharmony-tpc](https://gitee.com/openharmony-tpc), OpenHarmony的三方组件

## 社区基本文化
关于社区文化，笔者可能讲不清楚，但是有一个基本点就是：社区是靠不断的ISSUE鞭策改进，靠PR不断的完成改进；如果你发现某个社区很久没有ISSUE， 也很久没有PR，那么很可能，这个社区走向不活跃状态。
![](./media/仓库基本介绍.png)

## 作业

  如果觉得以下仓库还不错，可以一键三连（watch/star/fork）。
- [知识体系首页仓库](https://gitee.com/openharmony-sig/knowledge)
- [知识体系样例仓库-智能家居场景](https://gitee.com/openharmony-sig/knowledge_demo_smart_home)
- [知识体系样例仓库-影音娱乐场景](https://gitee.com/openharmony-sig/knowledge_demo_entainment)
- [知识体系样例仓库-购物消费场景](https://gitee.com/openharmony-sig/knowledge_demo_shopping)
- [知识体系样例仓库-运动健康场景](https://gitee.com/openharmony-sig/knowledge_demo_temp)
- [知识体系样例仓库-智能出行场景](https://gitee.com/openharmony-sig/knowledge_demo_travel)
- [知识体系样例仓库-智慧办公场景](https://gitee.com/openharmony-sig/knowledge_demo_temp)
- [知识体系样例仓库-其他场景](https://gitee.com/openharmony-sig/knowledge_demo_temp)
- [OpenHarmony赛事活动作品](https://gitee.com/openharmony-sig/online_event)

## 思考题
**想一想,OpenHarmony社区和gitee是什么关系？**

**[回到首页](../README_zh.md)**




