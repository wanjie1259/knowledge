# 如何贡献一个C/C++三方库

## 简介

 三方库指其他公司或者组织提供的服务或者模块，能持续繁荣 OpenAtom OpenHarmony（简称“OpenHarmony”）的生态建设 。 本文介绍如何在OpenHarmony标准系统上移植一个C/C++三方库并将该三方库提交到社区。



## 三方库移植问题与解决方案



![移植问题分析](media/requirement.png)

由上图可以看出，三方库移植最主要的两个问题：运行时依赖与编译结构不兼容。

#### 运行时依赖

运行时依赖主要包括系统API,运行C库以及硬件相关。一般的库都不会有这方面的依赖，除非一些专门针对硬件处理的库(比如针对GPU加速)，介于这硬件依赖关系，本文不对这类库做详细讲解，我们只需通过API扫描工具对库进行扫描，分析是否有这类依赖。

#### 编译结构不兼容

如今的编译结构种类繁多，如何加入到OpenHarmony编译构建系统，这是移植过程中比较常见，也是无法避免的一个问题。OpenHarmony编译构建如今支持几种形式：

1. 纯GN编译
2. GN加action/exec_script

现今大部分的三方库都是通过cmake编译，我们是可以通过上面第二种方式加入到OpenHarmony的编译构建系统中的。但是在标准系统中，通过action方式无法自动将生成的库文件拷贝到系统固件中，因此本文将重点介绍通过

分析cmake过程，手动构建GN规则进行移植并编译一个三方库。



## 三方库移植流程

三方库的移植大致可以分为4个部分，如下图所示：

![移植流程](media/porting.png)

接下来我们将以手动构建gn的方式移植openjpeg到OpenHamony 3.2 Beta1为例具体介绍整个移植流程。

验证该库的开发板我们选用rk3568.

关于OpenHamony 3.2 Beta代码下载参考官方文档:[OpenHarmony 3.2 Beta1](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.2-Beta1/zh-cn/release-notes/OpenHarmony-v3.2-beta1.md)。

环境搭建参照：[润和RK3568开发板标准设备快速上手](https://gitee.com/openharmony-sig/knowledge_demo_temp/tree/master/docs/rk3568_helloworld).

#### 原生库准备

##### 下载代码

 在 OpenHarmony/third_party 目录下，输入以下命令下载 openjpeg的源代码(建议下载最新分支代码)： 

```
cd OpenHarmony/third_party/
git clone git@github.com:uclouvain/openjpeg.git -b openjpeg-2.1
```

##### 生成Makefile

 从源码目录结构可以分析出，openjpeg是通过Cmake方式进行编译的，所以我们生成Makefile的话，需要执行以下几个步骤：

```
cd OpenHarmony/third_party/openjpeg
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
```

##### 分析编译依赖

###### 原文件依赖分析

执行make指令进行编译，通过编译过程可以分析对应库以及可执行文件需要的依赖文件:

```

water@ubuntu:~/openjpeg/openjpeg/build$ make
[  1%] Building C object src/lib/openjp2/CMakeFiles/openjp2_static.dir/thread.c.o
[  2%] Building C object src/lib/openjp2/CMakeFiles/openjp2_static.dir/bio.c.o
[  4%] Building C object src/lib/openjp2/CMakeFiles/openjp2_static.dir/cio.c.o
[  5%] Building C object src/lib/openjp2/CMakeFiles/openjp2_static.dir/dwt.c.o
[  7%] Building C object src/lib/openjp2/CMakeFiles/openjp2_static.dir/event.c.o
[  8%] Building C object src/lib/openjp2/CMakeFiles/openjp2_static.dir/ht_dec.c.o
[ 10%] Building C object src/lib/openjp2/CMakeFiles/openjp2_static.dir/image.c.o
[ 11%] Building C object src/lib/openjp2/CMakeFiles/openjp2_static.dir/invert.c.o
[ 13%] Building C object src/lib/openjp2/CMakeFiles/openjp2_static.dir/j2k.c.o
[ 14%] Building C object src/lib/openjp2/CMakeFiles/openjp2_static.dir/jp2.c.o
[ 16%] Building C object src/lib/openjp2/CMakeFiles/openjp2_static.dir/mct.c.o
[ 17%] Building C object src/lib/openjp2/CMakeFiles/openjp2_static.dir/mqc.c.o
[ 19%] Building C object src/lib/openjp2/CMakeFiles/openjp2_static.dir/openjpeg.c.o
[ 20%] Building C object src/lib/openjp2/CMakeFiles/openjp2_static.dir/opj_clock.c.o
[ 22%] Building C object src/lib/openjp2/CMakeFiles/openjp2_static.dir/pi.c.o
[ 23%] Building C object src/lib/openjp2/CMakeFiles/openjp2_static.dir/t1.c.o
[ 25%] Building C object src/lib/openjp2/CMakeFiles/openjp2_static.dir/t2.c.o
[ 26%] Building C object src/lib/openjp2/CMakeFiles/openjp2_static.dir/tcd.c.o
[ 28%] Building C object src/lib/openjp2/CMakeFiles/openjp2_static.dir/tgt.c.o
[ 29%] Building C object src/lib/openjp2/CMakeFiles/openjp2_static.dir/function_list.c.o
[ 31%] Building C object src/lib/openjp2/CMakeFiles/openjp2_static.dir/opj_malloc.c.o
[ 32%] Building C object src/lib/openjp2/CMakeFiles/openjp2_static.dir/sparse_array.c.o
[ 34%] Linking C static library ../../../bin/libopenjp2.a
[ 34%] Built target openjp2_static
[ 35%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/thread.c.o
[ 37%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/bio.c.o
[ 38%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/cio.c.o
[ 40%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/dwt.c.o
[ 41%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/event.c.o
[ 43%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/ht_dec.c.o
[ 44%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/image.c.o
[ 46%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/invert.c.o
[ 47%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/j2k.c.o
[ 49%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/jp2.c.o
[ 50%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/mct.c.o
[ 52%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/mqc.c.o
[ 53%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/openjpeg.c.o
[ 55%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/opj_clock.c.o
[ 56%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/pi.c.o
[ 58%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/t1.c.o
[ 59%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/t2.c.o
[ 61%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/tcd.c.o
[ 62%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/tgt.c.o
[ 64%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/function_list.c.o
[ 65%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/opj_malloc.c.o
[ 67%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/sparse_array.c.o
[ 68%] Linking C shared library ../../../bin/libopenjp2.so
[ 68%] Built target openjp2
[ 70%] Building C object src/bin/jp2/CMakeFiles/opj_dump.dir/opj_dump.c.o
[ 71%] Building C object src/bin/jp2/CMakeFiles/opj_dump.dir/convert.c.o
[ 73%] Building C object src/bin/jp2/CMakeFiles/opj_dump.dir/convertbmp.c.o
[ 74%] Building C object src/bin/jp2/CMakeFiles/opj_dump.dir/index.c.o
[ 76%] Building C object src/bin/jp2/CMakeFiles/opj_dump.dir/__/common/color.c.o
[ 77%] Building C object src/bin/jp2/CMakeFiles/opj_dump.dir/__/common/opj_getopt.c.o
[ 79%] Linking C executable ../../../bin/opj_dump
[ 79%] Built target opj_dump
[ 80%] Building C object src/bin/jp2/CMakeFiles/opj_compress.dir/opj_compress.c.o
[ 82%] Building C object src/bin/jp2/CMakeFiles/opj_compress.dir/convert.c.o
[ 83%] Building C object src/bin/jp2/CMakeFiles/opj_compress.dir/convertbmp.c.o
[ 85%] Building C object src/bin/jp2/CMakeFiles/opj_compress.dir/index.c.o
[ 86%] Building C object src/bin/jp2/CMakeFiles/opj_compress.dir/__/common/color.c.o
[ 88%] Building C object src/bin/jp2/CMakeFiles/opj_compress.dir/__/common/opj_getopt.c.o
[ 89%] Linking C executable ../../../bin/opj_compress
[ 89%] Built target opj_compress
[ 91%] Building C object src/bin/jp2/CMakeFiles/opj_decompress.dir/opj_decompress.c.o
[ 92%] Building C object src/bin/jp2/CMakeFiles/opj_decompress.dir/convert.c.o
[ 94%] Building C object src/bin/jp2/CMakeFiles/opj_decompress.dir/convertbmp.c.o
[ 95%] Building C object src/bin/jp2/CMakeFiles/opj_decompress.dir/index.c.o
[ 97%] Building C object src/bin/jp2/CMakeFiles/opj_decompress.dir/__/common/color.c.o
[ 98%] Building C object src/bin/jp2/CMakeFiles/opj_decompress.dir/__/common/opj_getopt.c.o
[100%] Linking C executable ../../../bin/opj_decompress
[100%] Built target opj_decompress
```

由上面过程可看出，每个编译目标(Built target)所依赖的文件由上一个目标(如果上一个目标由的话)生成后以及该目标生成前的文件组成，如生成libopenjp2.so（Built target openjp）的文件是Built target openjp2_static之后以及Built target openjp之前的文件：

```
[ 35%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/thread.c.o
[ 37%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/bio.c.o
[ 38%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/cio.c.o
[ 40%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/dwt.c.o
[ 41%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/event.c.o
[ 43%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/ht_dec.c.o
[ 44%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/image.c.o
[ 46%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/invert.c.o
[ 47%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/j2k.c.o
[ 49%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/jp2.c.o
[ 50%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/mct.c.o
[ 52%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/mqc.c.o
[ 53%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/openjpeg.c.o
[ 55%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/opj_clock.c.o
[ 56%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/pi.c.o
[ 58%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/t1.c.o
[ 59%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/t2.c.o
[ 61%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/tcd.c.o
[ 62%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/tgt.c.o
[ 64%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/function_list.c.o
[ 65%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/opj_malloc.c.o
[ 67%] Building C object src/lib/openjp2/CMakeFiles/openjp2.dir/sparse_array.c.o
```

###### 依赖库及 cflag 标志 分析

执行make编译完后，在build的目录下会生成对应源码目录结构的目录，且在该目录CMakeFiles/xxx.dir内会有build.make以及 depend.make、flags.make， link.txt  。通过分析这四个文件我们可以找到对应的依赖库以及cflag 标志。

| build.make  |   生成目标的源文件    |
| :---------: | :-------------------: |
| depend.make | 需要依赖的其它C源文件 |
| flags.make  | cflags相关的一些标记  |
|  link.txt   |    链接库相关信息     |



如openjpeg中生成libopenjp2的源码目录下是src/lib/openjp2，则build中会生成对应的文件夹src/lib/CMakeFiles/openjp2 .dir，且该文件夹中可以查看到 build.mak ，depend.make、flags.make，link.txt四个文件，其相关内容如下：

depend.make：

```
# Empty dependencies file for openjp2.
# This may be replaced when dependencies are built.
```

flags.make：

```
# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.16

# compile C with /usr/bin/cc
C_FLAGS = -fPIC   -Wall -Wextra -Wconversion -Wunused-parameter -Wdeclaration-after-statement -Werror=declaration-after-statement

C_DEFINES = -DMUTEX_pthread -Dopenjp2_EXPORTS

C_INCLUDES = -I/home/water/openjpeg/openjpeg/build/src/lib/openjp2
```

link.txt：

```
/usr/bin/cc -fPIC -O3 -DNDEBUG  -shared -Wl,-soname,libopenjp2.so.7 -o ../../../bin/libopenjp2.so.2.5.0 CMakeFiles/openjp2.dir/thread.c.o CMakeFiles/openjp2.dir/bio.c.o CMakeFiles/openjp2.dir/cio.c.o CMakeFiles/openjp2.dir/dwt.c.o CMakeFiles/openjp2.dir/event.c.o CMakeFiles/openjp2.dir/ht_dec.c.o CMakeFiles/openjp2.dir/image.c.o CMakeFiles/openjp2.dir/invert.c.o CMakeFiles/openjp2.dir/j2k.c.o CMakeFiles/openjp2.dir/jp2.c.o CMakeFiles/openjp2.dir/mct.c.o CMakeFiles/openjp2.dir/mqc.c.o CMakeFiles/openjp2.dir/openjpeg.c.o CMakeFiles/openjp2.dir/opj_clock.c.o CMakeFiles/openjp2.dir/pi.c.o CMakeFiles/openjp2.dir/t1.c.o CMakeFiles/openjp2.dir/t2.c.o CMakeFiles/openjp2.dir/tcd.c.o CMakeFiles/openjp2.dir/tgt.c.o CMakeFiles/openjp2.dir/function_list.c.o CMakeFiles/openjp2.dir/opj_malloc.c.o CMakeFiles/openjp2.dir/sparse_array.c.o  -lm -lpthread 
```

从以上文件可以分析出 libopenjp2.so依赖库以及 C_FLAGS 信息 

```
需要链接的库：
 -lm -lpthread 
编译器需要添加的C_FLAGS标记：
 C_FLAGS = -O3 -DNDEBUG -fPIC   -ffast-math -Wall -Wextra -Wconversion -Wunused-parameter -Wdeclaration-after-statement -Werror=declaration-after-statement
```

测试用例以及对应的静态库也是通过这种方法分析。



##### 构建gn

通过以上分析得出的信息，我们可以构建出基于OpenHarmony编译系统的libopenjp2.so的BUILD.gn文件。

在openjpeg目录新建一个BUILD.gn。

```
cd OpenHarmony/third_party/openjpeg
touch BUILD.gn
```

将上面分析的源文件以及依赖库和FLAG都添加到BUILD.gn中

```
import("//build/ohos.gni")

#编译CFLAG标记和需要链接的库
config("openjpeg_cflag_config") {
  cflags = [ 
      "-O3" ,
      "-DNDEBUG",
      "-fPIC",
      "-ffast-math",
      "-Wall",
      "-Wextra",
      "-Wconversion",
      "-Wunused-parameter",
      "-Wdeclaration-after-statement",
      "-Werror=declaration-after-statement",
      ]
    ldflags = [
        "-lm",
        "-lpthread",
    ]
}

#需要包含的头文件
config("openjpeg_config") {
    include_dirs = [
        "//third_party/openjpeg/src/lib/openjp2"
    ]
}

#需要包含的源文件
ohos_source_set("libopenjpeg_source") {
    sources = [
        "//third_party/openjpeg/src/lib/openjp2/thread.c",
        "//third_party/openjpeg/src/lib/openjp2/bio.c",
        "//third_party/openjpeg/src/lib/openjp2/cio.c",
        "//third_party/openjpeg/src/lib/openjp2/dwt.c",
        "//third_party/openjpeg/src/lib/openjp2/event.c",
        "//third_party/openjpeg/src/lib/openjp2/ht_dec.c",
        "//third_party/openjpeg/src/lib/openjp2/image.c",
        "//third_party/openjpeg/src/lib/openjp2/invert.c",
        "//third_party/openjpeg/src/lib/openjp2/j2k.c",
        "//third_party/openjpeg/src/lib/openjp2/jp2.c",
        "//third_party/openjpeg/src/lib/openjp2/mct.c",
        "//third_party/openjpeg/src/lib/openjp2/mqc.c",
        "//third_party/openjpeg/src/lib/openjp2/openjpeg.c",
        "//third_party/openjpeg/src/lib/openjp2/opj_clock.c",
        "//third_party/openjpeg/src/lib/openjp2/pi.c",
        "//third_party/openjpeg/src/lib/openjp2/t1.c",
        "//third_party/openjpeg/src/lib/openjp2/t2.c",
        "//third_party/openjpeg/src/lib/openjp2/tcd.c",
        "//third_party/openjpeg/src/lib/openjp2/tgt.c",
        "//third_party/openjpeg/src/lib/openjp2/function_list.c",
        "//third_party/linu/src/lib/openjp2/opj_malloc.c",
        "//third_party/openjpeg/src/lib/openjp2/sparse_array.c",
    ]
}

##生成动态库
ohos_shared_library("openjpeg") {
	deps = [ ":libopenjpeg_source" ]         #依赖的源文件
    public_configs = [ ":openjpeg_config" ]  #依赖的头文件配置信息
    configs = [ ":openjpeg_cflag_config" ]   #依赖的CFLAGS配置信息
    part_name = "openjpeg_lib"				
}
```

参照以上方法，可以构建出openjpg2所有测试用例的gn文件，以test_title_decoder为例：

```
cd OpenHarmony/third_party/openjpeg/tests/
touch BUILD.gn
```

根据测试用例的依赖分析添加以下代码到BUILD.gn中：

```
import("//build/ohos.gni")

#需要包含的头文件
config("openjpeg_config") {
    include_dirs = [
        "//third_party/openjpeg/src/lib/openjp2",
        "//third_party/openjpeg/src/bin/common",
        "//third_party/openjpeg/src/lib/jp2",
        "//third_party/openjpeg/src/bin/jp2",
    ]
}

##生成可执行文件
ohos_executable("test_title_decoder") {
	sources = [
		"test_title_decoder.c",
		"../src/bin/common/opj_getopt.c"
	]
	deps = [ "//third_party/openjpeg:openjpeg" ]         #依赖的openjp2库
    configs = [ ":openjpeg_config" ]  					 #依赖的头文件配置信息
    part_name = "openjpeg_lib"				
}
```

注意：其他测试用例也可以写到该gn文件中，以不同的ohos_executable名区分，如：

&nbsp;![build.gn](media/gn.png)

#### 加入编译构建体系

标准系统编译构建可以参考文档[标准系统编译构建指导](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.2-Beta1/zh-cn/device-dev/subsystems/subsys-build-standard-large.md).

##### 定义子系统并加入到编译框架

- 在系统源码根目录下创建一个目录作为子系统目录，子系统目录可创建在OpenHarmony源码目录任意位置。

  本项目以 third_party/openjpeg 做为子系统目录，子系统名字即为openjpeg 。

- 子系统目录下创建ohos.build文件，构建时会先读取该文件。

  ```
  ##third_party/openjpeg/ohos.build文件内容:
  
  {  
    "subsystem": "openjpeg", 
    "parts": {    
      "openjpeg_lib": {      
  	  "module_list":[        
  		"//third_party/openjpeg/tests:test_tile_decoder", #新增测试文 test_tile_decoder
          "//third_party/openjpeg/tests:test_tile_encoder", #新增测试文 test_tile_encoder
          ...........      								  #添加其他测试文件
        ]
      }  
    }
  }
  ```

  

- 把子系统配置到build/subsystem_config.json.

  ```
  "openjpeg": {    
    "path": "third_party/openjpeg",   
    "name": "openjpeg"  
  }
  ```

  

##### 定义组件并加入子系统

在定义子系统的时添加的ohos.build文件中，我们已经定义好了对应的组件openjpeg_lib，module_list所定义的为该组件的目标，目标可以为动态库，静态库，可执行文件。



##### 定义目标并加入组件

在构建gn的时候，我们定义的动态库以及可执行程序都是我们的目标模块，在每个目标模块中添加对应的组件名后即将我们的目标模块加入到该组件中：

```
part_name = "openjpeg_lib"
```



#### 产品引用

添加完子系统后，我们还需要将子系统及其组件加入产品定义中，以rk3568为例，产品定义文件存在vendor/hihope/rk3568/config.json，我们需要将以下内容添加到config.json中：

```
{
  "subsystem":"thirdparty",
  "component":[
    {
      "component":"musl",
      "features":[]
    }
  ]
},
## 以下新加子系统信息
{ 
  "subsystem":"openjpeg",
  "component":[
    {
      "component":"openjpeg_lib",
      "features":[]
    }
  ]
}
##
```

#### 编译

所有内容添加完后，执行以下命令进行编译：

```
hb set		## 选择产品类型，此处我们选择rk3568
hb build -f ## 全量编译，如果编译64位的系统，执行 hb build --target-cpu arm64
```

 编译完成以后可以在 /out/rk3568/packages/phone/system/lib 目录下看到编译生成的 libopenjpeg.z.so 文件。 

```
ls -l out/rk3568/packages/phone/system/lib/libopenjpeg.z.so
-rwxr-xr-x 1 root root 286148 Mar 23 18:59 libopenjpeg.z.so
```

 在out/rk3568/packages/phone/system/bin 目录下看到我们编译的测试文件 test_tile_encoder 等

```
ls -l out/rk3568/packages/phone/system/bin/test_*
-rwxr-xr-x 1 root root 286148 Mar 23 18:59 test_decode_area
-rwxr-xr-x 1 root root 286148 Mar 23 18:59 test_tile_decoder
-rwxr-xr-x 1 root root 286148 Mar 23 18:59 test_tile_encoder
```



#### 测试

 在开源库的 tests 目录下有很多测试用例，在 test 目录下的 CMakeLists.txt 文件会生成一些测试文件来进行验证，其中会给出一些测试方法，具体如下： 

![test1](media/test1.jpg)

![test2](media/test2.jpg)

将OpenHarmony生成的库文件，测试应用和测试文件打包，通过hdc_std命令将压缩包push到rk3568开发板，并执行对应的测试操作，具体步骤：

1. 重加载系统，为了可以将压缩包push到开发板

   ```
   hdc_std shell			## 进入开发板系统
   mount -o remount,rw /	## 重新加载系统为可读写
   mkdir openjpeg			## 创建openjpeg文件夹用来存放测试用例
   exit					## 退出开发板系统
   ```

2. 将压缩包push到开发板

   ```
   hdc_std file send openjpej.tar /openjpeg
   ```

3. 解压压缩包并将库文件拷贝到对应的目录

   ```
   hdc_std shell
   cd openjpeg
   tar -xvf openjpej.tar
   cp libopenjp2.z.so /system/lib/	## 如果系统为64位： cp libopenjp2.z.so /system/lib64
   ```

4. 执行测试程序

   ```
   ./test_tile_encoder 3 2000 2000 1000 1000 8 1 tte1.j2k 64 64 4 1 1 1 256 256
   ```

5. 测试结果输出

   ![test_result](media/result.png)

至此，整个库的移植就完成了。



## 三方库提交

移植完一个三方库后我们需要将该库提交到社区，[OpenHarmony   C/C++  三方库SIG仓](https://gitee.com/openharmony-sig/tpc_c_cplusplus)。目前该仓库存放移植完后三方库的相关适配脚本文件。

### 提交内容

- README.OpenSource，记录三方库的开源信息的文档，基本内容如下：

  ```
  [
      {
        "Name": "",				## 库名
        "License": "",			## 开源协议
        "License File": ",		## 开源文件，一般开源项目都会自带该文件
        "Version Number": "",		## 库的版本
        "Owner": "",				## 作者
        "Upstream URL": "",		## 开源库的地址
        "Description": ""			## 库的描述
      }
  ]
  ```

- 适配编译脚本

  适配脚本分为2种

  1. gn脚本：适合OpenHarmony源码上编译的应用调用的三方库，此时三方库编译的so最终会打包到rom中。
  2. cmake脚本：适合IDE上开发NAPI接口时调用，此时三方库编译的so会和应用一起打包到hap中。

- 配置文件

  原生库通过make或者cmake时生成的一些配置文件。如比较通用的config.h配置文件，config.h文件为原生库生成文件，原则上也不做修改，为了避免文件上仓进行代码扫码，可以将文件文件修改后缀名(config.h.in)。相关文档中需要将配置文件使用方法说明。

- 改动的代码文件

  如果移植的三方库中涉及到OpenHarmony不支持的API或驱动，需要重新适配相关代码，适配后的代码需要提交。

- 适配使用说明文档

  文档需要明确以下几点：

  1. 三方库选择的版本。
  2. 适配的OpenHarmony的版本/或者API的版本
  3. 对于IDE上使用的三方库还需要说明IDE的版本以及对应工程中的model的配置
  4. 涉及到工具链的需要提供工具链版本
  5. 文件有修改的需要说明修改点以及修改的原因
  6. 配置文件的使用

- 涉及源码提交的还需要提供OAT.xml， OAT开源审查配置，详细信息参考[ OAT开源审查](https://gitee.com/openharmony-sig/tools_oat).



### 提交步骤

- fork [OpenHarmony  C/C++ 三方库SIG仓](https://gitee.com/openharmony-sig/tpc_c_cplusplus)到本地。
- 将准备好的提交内容push到本地仓库。
- 本地仓库提PR到主仓。

详细步骤可以参照[如何提交PR的指导文档](https://gitee.com/zhong-luping/knowledge/blob/master/docs/openharmony_getstarted/push_pr/readme.md) 。

## FAQ

- 我执行make指编译的时候，没有打印对应的目标及其依赖文件该如何处理？

   分析编译依赖时有提到过build.make文件，该文件中就有目标的定义以及依赖源文件。

   如上面提到的生成openjp2.so会在build下生成src/lib/openjp2/CMakeFiles/openjp2.dir/build.make,在该文件中可以找到如下内容：

   ![build.make](media/build_make.png)

- 我移植的库没有cmake怎么办？

   本文重点介绍通过cmake构建gn来移植三方库，针对其他方式可以通过分析Makefile文件来构建gn，具体方法可以参照[三方库移植依赖分析](https://gitee.com/huangminzhong/mytest/tree/master/OpenHarmony_ThirdParty)。

- 编译的时候提示XXX接口没有定义怎么办？

   出现此问题有以下几点需要确认：

   1. 确认源文件是否添加完全
   2. 确认是否添加头文件以及头文件路径



## 参考资料

[OpenHarmony 3.2 Beta1简介](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.2-Beta1/zh-cn/release-notes/OpenHarmony-v3.2-beta1.md)

[润和RK3568开发板标准设备快速上手](https://gitee.com/openharmony-sig/knowledge_demo_temp/tree/master/docs/rk3568_helloworld)

[标准系统编译构建指导](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.2-Beta1/zh-cn/device-dev/subsystems/subsys-build-standard-large.md)

[OpenHarmony C/C++三方库SIG仓](https://gitee.com/openharmony-sig/tpc_c_cplusplus)

[OAT开源审查](https://gitee.com/openharmony-sig/tools_oat).

[如何提交PR的指导文档](https://gitee.com/zhong-luping/knowledge/blob/master/docs/openharmony_getstarted/push_pr/readme.md) 

[知识体系](https://gitee.com/openharmony-sig/knowledge)