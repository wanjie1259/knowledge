# OpenHarmony知识体系工作组
OpenHarmony知识体系工作组汇聚了19家合作伙伴，为OpenHarmony社区共建学习路径，开发样例，三方库，深度文章，直播课程，素材拓展等内容，并依托[OpenHarmony官网](https://www.openharmony.cn)呈现给开发者，帮助开发者快速查找学习资源，共同繁荣OpenHarmony社区。

关于开发样例上官网、学习路劲上官网、直播课程共建以及其他需要知识体系工作组提供支持的活动都需要在知识体系工作组的双周例会上进行决策；知识体系双周例会详情可以参考[OpenHarmony知识体系SIG双周例会议题申报](https://docs.qq.com/sheet/DUUNpcWR6alZkUmFO)

![image-20220323205725142](media/knownlege_range2.png)

## 开发样例

OpenHarmony开发样例，主要分为以突出API使用范例的Samples，按步骤的交互方式指导开发者的Codelabs，以及来自广大社区伙伴分享的场景化Demos。

#### Samples

  请参考[Samples](https://gitee.com/openharmony/app_samples)

#### Codelabs

  请参考[Codelabs](https://gitee.com/openharmony/codelabs)

#### 场景化Demos

​	场景化Demo目前是按照各个Demo对应的场景来分类的，目前场景Demo的分类如下：

- [智能家居场景](https://gitee.com/openharmony-sig/knowledge_demo_smart_home)
- [影音娱乐场景](https://gitee.com/openharmony-sig/knowledge_demo_entainment)
- [购物消费场景](https://gitee.com/openharmony-sig/knowledge_demo_shopping)
- [运动健康场景](https://gitee.com/openharmony-sig/knowledge_demo_temp)
- [智能出行场景](https://gitee.com/openharmony-sig/knowledge_demo_travel)
- [智慧办公场景](https://gitee.com/openharmony-sig/knowledge_demo_temp)
- [其他场景](https://gitee.com/openharmony-sig/knowledge_demo_temp)
- [赛事活动作品](https://gitee.com/openharmony-sig/online_event)

#### 开发样例上官网

为了让开发者更便捷的找到自己需要的样例，也为了更好呈现各个开发者贡献的开发样例，知识体系工作组在[OpenHarmony 官网](https://www.openharmony.cn)开辟了开发样例板块。开发者可以将自己的优秀作品推荐到官网，流程参考[开发样例上官网流程](./media/demo_on_official_website_flow.png)

#### 欢迎共建样例

欢迎大家积极参与OpenHarmony开发样例共建，为社区做贡献，也Show出自己的实力。关于如何参与贡献可以参考[OpenHarmony共建开发样例说明](docs/co-construct_demos/README_zh.md)

## 学习路径

开发者根据自己的情况，选择适合自己的学习路径，学习OpenHarmony开发。

浏览[OpenHarmony官网](https://www.openharmony.cn)查找学习路径。

## 直播课程

为方便开发者更好的上手OpenHarmony，知识体系工作组联合各个共建单位、运营小组、基础设施组打造了知识赋能直播课程。针对OpenHarmony的各个子系统、编译构建、移植以及相关技术点，手术刀似的进行公开讲解，为广大开发者答疑解惑。后续也会基于直播课程素材做学习路径等。同时我们也欢迎广大开发者和我们一起做好这个赋能课程，如果你有意愿，欢迎加入知识体系直播课程队伍，详情参考[OpenHarmony直播课程共建流程](
https://gitee.com/openharmony-sig/sig-content/blob/master/knowlege/meetings/2022-3-8-meeting.md)。

## 文章博客

在[OpenHarmony 官网](https://www.openharmony.cn)博客模块中，收集了各类深度文章、样例解析等技术文章，方便开发者深入浅出的学习相关技术。同时如何想要show自己技术文章也欢迎大家进行共建，流程参考[OpenHarmony共建知识体系文章流程](https://docs.qq.com/slide/DUWFGTGhTUkhJaktB)。

## 三方库
- [OpenHarmony三方库TPC仓](https://gitee.com/openharmony-tpc)

## 参考资料

- [OpenHarmony官网](https://www.openharmony.cn/)
- [OpenHarmony官网开发样例](https://growing.openharmony.cn/mainPlay/content/mainText/allFeatures)
- [OpenHarmony知识体系SIG双周例会议题申报](https://docs.qq.com/sheet/DUUNpcWR6alZkUmFO)
- [OpenHarmony共建开发样例说明](docs/co-construct_demos/README_zh.md)
- [OpenHarmony知识体系SIG双周例会纪要](https://gitee.com/openharmony-sig/sig-content/tree/master/knowlege/meetings)
- [OpenHarmony样例上官网流程](./media/demo_on_official_website_flow.png)
- [OpenHarmony学习路径上官网流程](media/learnpath.png)
- [OpenHarmony直播课程共建流程](
  https://gitee.com/openharmony-sig/sig-content/blob/master/knowlege/meetings/2022-3-8-meeting.md)
- [OpenHarmony共建知识体系文章流程](https://docs.qq.com/slide/DUWFGTGhTUkhJaktB)